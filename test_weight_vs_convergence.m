% This file generates figure [?] in 
% Reinier Doelman, Michel Verhaegen - Convex optimization-based blind
% deconvolution for images taken with coherent illumination. submitted to
% JOSA

% Script author: Reinier Doelman
% Date: 11-2-2019

% Requirements:
% - MATLAB. I used R2018a.
% - YALMIP optimization modelling software (yalmip.github.io)
% - A semidefinite programming solver. I used the commercial MOSEK solver (mosek.com).

% The script runs an algorithm for blind deconvolution for the case
% of coherent illumination (Algorithm 1 in the paper) with different
% weighting parameters to investigate their influence on the speed of convergence.

% The deconvolution problem is the following:
% Find u,v such that yi = |conv(u,Bi*v)|^2 where Bi is given for i = 1:d.
% This script implements blind deconvolution for the 1D case.
% The notation is different from the paper, with the following
% correspondence:
% `g_o' is renamed to `u' (the complex field in the object plane)
% `g_i' is renamed to `w' (the complex field in the image/focal plane)

% Since the case is 1 dimensional, we can write the convolution as follows:
% w = conv(u,Bi*v) = T(u)*Bi*v, where T(u) creates the toeplitz matrix for the
% vector u assiciated with convolution.
% We therefore obtain the following bilinear constraints for each diversity:
% y_j = w_j' * w_j for j = 1:m+p-1
% w = T(u)*Bi*v
% In this script we use these formulations to form the matricese M_m and
% M_c

% == clean and fix random number generator for reproduceability.
clear all
close all
clc

rng(123)

% == parameter settings
m = 4; % length u
n = 2; % length v
p = 3; % length B*v
d = 3; % diversities

% == the true solution
uo = 2*(rand(m,1)+1i*rand(m,1)) - (1+1i); % true vector u
vo = 2*(rand(n,1)+1i*rand(n,1)) - (1+1i); % true vector v

B = randn(p,n,d)+1i*randn(p,n,d); % Bi = B(:,:,i)

% the measurement vector y is calculated by convolution and taking the
% squared absolute value.
wo = zeros(m+p-1,d);
for i = 1:d
    wo(:,i) = conv(uo,B(:,:,i)*vo);
end
y = abs(wo).^2;

% == Experiment settings
% The initial guesses in this examample are perturbations of the true
% solutions.
scaling = 0.8;
xu = -uo + scaling*(randn(m,1)+1i*randn(m,1));
xv = -vo + scaling*(randn(n,1)+1i*randn(n,1));
xw = -wo + scaling*(randn(m+p-1,d)+1i*randn(m+p-1,d));

% The length of the simulation and the weights that are used.
N = 60; % Perform 60 iterations
lambda = [1 1 1];     % lambda(i)   is the setting for lambda   in the i'th experiment. The paper uses the letter mu for this parameter.
u_weight = [1 1 1];   % u_weight(i) is the setting for u_weight in the i'th experiment.
v_weight = [1 4 0.6]; % v_weight(i) is the setting for v_weight in the i'th experiment.
w_weight = [1 2 0.6]; % w_weight(i) is the setting for w_weight in the i'th experiment.

s = length(lambda); % The number of different parameter settings that are tested. We test 3 different combinations.

% For every iteration and every parameter combination, we record the values of:
% - lambda*||M_m||_* + ||M_c||_* - (m+p-1) - sum_i || B_i ||_*
% - ||M_m||_* - (m+p-1)
% - ||M_c||_* - sum_i || B_i ||_*
% If the equality constraints hold, these values are zero (in practice, numerically small).
% Here we initialize this variable.
nuc_fit = zeros(N,3,s);

% For every iteration and every parameter combination, we record the values of:
% - lambda*||y - yh||_2^2 + ||wh - conv(uh,Bi*vh)||_2^2
% - ||y - yh||_2^2
% - ||wh - conv(uh,Bi*vh)||_2^2
% If the equality constraints hold, these values are zero (in practice, numerically small).
% Here we initialize this variable.
fro_fit = zeros(N,3,s);

% The function test_combination_weights runs algorithm 1 with specific
% weights and returns the performance associated to these weights.
for i = 1:s
    [nf,ff] = test_combination_of_weights(m,n,p,d,y,B,xu,xv,xw,lambda(i),u_weight(i),v_weight(i),w_weight(i),N);
    nuc_fit(:,:,i) = nf;
    fro_fit(:,:,i) = ff;
end

f = figure(1);
    pl = plot(1:N,util.rvecn(util.vec(fro_fit(:,2:3,:)),N)); % plot the frobenius norm fit
    pl = reshape(pl,2,s);
    ls = {'-',':'};
    for i = 1:s
        pl(1,i).LineStyle = ls{1}; % measurement fit, ||y - yh||_2^2
        pl(2,i).LineStyle = ls{2}; % convolution fit, ||wh - conv(uh,Bi*vh)||_2^2
    end
    for i = 1:s
        pl(1,i).Color = [0 0 0]; % black
        pl(2,i).Color = [0 0 0]; % black
    end
    for i = 1:2*s
        pl(i).LineWidth = 0.58;
    end
    
    % indicators
    text(23.2,1E-10,'\leftarrow I')
    text(9.8,1E-10,'\leftarrow II')
    text(46.7,1E-10,'\leftarrow III')
    
    set(gca,'YScale','log')
    leg = legend([pl(1,1) pl(2,1)],{'Measurement fit','Convolution fit'});
    leg.Box = 'off';
    
    title('Measurement and convolution fit for three sets of weights')
    xlabel('Iteration')
    ylabel('Fit')
    f.Position(3:4) = [560 327];