function [nuc_fit,fro_fit] = test_combination_of_weights(m,n,p,d,y,B,xu,xv,xw,lambda,u_weight,v_weight,w_weight,N)
%TEST_WEIGHT Summary of this function goes here
%   Detailed explanation goes here

nuc_fit = zeros(N,3);
fro_fit = zeros(N,3);
settings = sdpsettings('solver','mosek','verbose',0); % These are settings to pick the solver and to suppress output.

% This anonymous function creates the toeplitz matrix associated with
% convolution, conv(u,v) = T(u)*v
T = @(uu) toeplitz([uu.' zeros(1,p-1)],[uu(1) zeros(1,p-1)]);

% This anonymous function creates a sparse diagonal matrix of size m+p-1 with the values
% of x on the diagonal.
dd = @(x) sparse(1:(m+p-1),1:(m+p-1),x);

% These anonymous functions create the matrices M_m and M_c that include
% the weights
Mc = @(BB,uu,vv,xx,yy,ww) [v_weight*(ww+T(uu)*BB*yy+T(xx)*BB*vv+T(xx)*BB*yy)*u_weight T(uu+xx)*BB*u_weight; BB*(vv+yy)*v_weight BB];
Mm = @(yy,ww,xx) [w_weight*(dd(yy)+dd(ww)'*dd(xx)+dd(xx)'*dd(ww)+dd(xx)'*dd(xx))*w_weight w_weight*dd(ww+xx)'; dd(ww+xx)*w_weight speye((m+p-1))];

for k = 1:N
    k
    
    % The optimization in Equation 25 is implemented using Yalmip.
    yalmip('clear') % This prevents a growing log of variables created with Yalmip
    u = sdpvar(m,1,'full','complex');
    v = sdpvar(n,1,'full','complex');
    w = sdpvar(m+p-1,d,'full','complex');
    
    obj1 = 0;
    obj2 = 0;
    matrices_Mm = [];
    matrices_Mc = [];
    for j = 1:d
        matrices_Mm = cat(3,matrices_Mm,Mm(y(:,j),w(:,j),xw(:,j)));
        matrices_Mc = cat(3,matrices_Mc,Mc(B(:,:,j),u,v,xu,xv,w(:,j)));
        obj1 = obj1 + norm(matrices_Mm(:,:,j),'nuclear') - (m+p-1);
        obj2 = obj2 + norm(matrices_Mc(:,:,j),'nuclear') - sum(svd(B(:,:,j)));
    end
    obj = lambda*obj1 + obj2;
    di = optimize([],obj,settings);
    
    % compute fits
    nuc_fit(k,:) = [value(obj) value(obj1) value(obj2)];
    wh = zeros(m+p-1,d);
    for j = 1:d
        wh(:,j) = conv(value(u),B(:,:,j)*value(v));
    end
    yh = abs(value(w)).^2;
    fro_fit(k,[2 3]) = [norm(y - yh,'fro').^2 norm(wh - value(w),'fro').^2];
    fro_fit(k,1) = lambda*fro_fit(k,2) + fro_fit(k,3);
        
    xu = -value(u);
    xv = -value(v);
    xw = -value(w);

end

end

